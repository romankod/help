﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApplication1
{
    static class Program
    {
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            var initBd = new InitBD(); // добввляємо статичні готелі
            initBd.AddSomeStaticStuff();    //
            Application.Run(new start());
        }
    }
}
