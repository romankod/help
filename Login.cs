﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApplication1
{
    public partial class Login : Form
    {
        public Login()
        {
            InitializeComponent();
        }

        private void Login_Load(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            start go = new start();
            go.Show();
            this.Hide();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            // перевіряємо заповненість полів
            if (textBox1.Text != "" && textBox2.Text != "")
            {
                string Login_f = textBox1.Text;   // записуєм логін та пароль
                string Password_f = textBox2.Text;
                Clients client = new Clients(); // об'єкт класу
                bool successful = client.LogIn(Login_f , Password_f); // викликаємо метод логінування і записуємо результат в змінну
                if (successful == true) // якщо логінування трушне, то переходимо далі
                {
                    Main go = new Main(); // переходимо до наступної форми
                    go.Show();
                    this.Hide();
                }
                else
                {
                    MessageBox.Show("неправильний логін або пароль");
                }

            }
            else
            {
                MessageBox.Show("Перевірте поля");
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            Application.Exit();     // кнопка виход
        }
    }
}
