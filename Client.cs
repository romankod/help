﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsFormsApplication1
{
    public class Clients
    {
        public string firstName { get; set; }
        public string lastName { get; set; }
        public string login { get; set; }
        public string password { get; set; }
        public int money { get; set; }
        public string Login_f { get; set; }
        public string Password_f { get; set; }

        public Clients()
        {
        }
        public Clients(string firstName, string lastName, string login, string password, int money)
        {
            this.firstName = firstName;
            this.lastName = lastName;
            this.login = login;
            this.password = password;
            this.money = money;
            this.Login_f = Login_f;
            this.Password_f = Password_f;
            //Data.Clients.Add(this);

        }

        public static void CLient_reg(string firstName, string lastName, string login, string password, int money)
        {
            Data.Clients.Add(new Clients(firstName, lastName, login, password, money));
        }

        public bool LogIn(string Login_f, string Password_f)    // метод для логування та запису поточного кліжнта
        {
            foreach (var client in Data.Clients)
            {
                if (client.password == Password_f && client.login == Login_f)
                {
                    CurrentUser.getInstance().currentclient = client;  // запис поточного клієнта
                    return true;    // повертаєм трушне значення 
                }
                else
                {
                    return false;
                }
            }
            return false;
        }

        public bool currentUserEdit (string firstName_e, string lastName_e, string login_e, string password_e, int money_e)
        {
            foreach (var client in Data.Clients)
            {
                if (client.login == login_e)
                {
                    return false;
                }
                else
                {
                    CurrentUser.getInstance().currentclient.firstName = firstName_e; // перезаписуєм дані
                    CurrentUser.getInstance().currentclient.lastName = lastName_e;
                    CurrentUser.getInstance().currentclient.money = money_e;
                    CurrentUser.getInstance().currentclient.login = login_e;
                    CurrentUser.getInstance().currentclient.password = password_e;
                    return true;
                }
            }
            return false;
        }

    };

}
