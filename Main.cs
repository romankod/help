﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApplication1
{
    public partial class Main : Form
    {
        public Main()
        {
            InitializeComponent();
            label3.Text=CurrentUser.getInstance().currentclient.firstName.ToString(); // виводимо дані про поточного юзера
            label4.Text = CurrentUser.getInstance().currentclient.lastName.ToString();
            label5.Text = CurrentUser.getInstance().currentclient.money.ToString();
        }

        private void listView1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void Main_Load(object sender, EventArgs e)
        {
            listView1.Visible = true;   // виводимо інформацію про юзерів
            listView1.View = View.Details;
            listView1.GridLines = true;
            listView1.FullRowSelect = true;
            listView1.Columns.Add("Name", 100);
            listView1.Columns.Add("Last name", 100);
            listView1.Columns.Add("money", 100);
            foreach (var client in Data.Clients)
            {
                ListViewItem itm;
                String[] Clients_list = new String[3];
                Clients_list[0] = client.firstName.ToString();
                Clients_list[1] = client.lastName.ToString();
                Clients_list[2] = client.money.ToString();
                itm = new ListViewItem(Clients_list);
                listView1.Items.Add(itm);
            }

            listView2.Visible = true;   // виводимо інфу про готелі
            listView2.View = View.Details;
            listView2.GridLines = true;
            listView2.FullRowSelect = true;
            listView2.Columns.Add("Name", 100);
            listView2.Columns.Add("Location", 100);
            listView2.Columns.Add("room count", 100);
            listView2.Columns.Add("price", 100);
            foreach (var hotel in Data.Hotels)
            {
                ListViewItem itm;
                String[] Hotel_list = new String[4];
                Hotel_list[0] = hotel.Name.ToString();
                Hotel_list[1] = hotel.Adress.ToString();
                Hotel_list[2] = hotel.roomCount.ToString();
                Hotel_list[3] = hotel.price.ToString();
                itm = new ListViewItem(Hotel_list);
                listView2.Items.Add(itm);
            }
        }

        private void label1_Click_1(object sender, EventArgs e)
        {

        }

        private void listView2_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            Form1 go = new Form1(); // додавання нового готелю
            go.Show();
            this.Hide();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            Application.Exit(); // кнопка виходу
        }

        private void button2_Click(object sender, EventArgs e)
        {
            client_reg go = new client_reg();   // одавання нового клієнта
            go.Show();
            this.Hide();
        }

        private void label2_Click(object sender, EventArgs e)
        {
           
        }

        private void label4_Click(object sender, EventArgs e)
        {

        }

        private void label5_Click(object sender, EventArgs e)
        {

        }

        private void label3_Click(object sender, EventArgs e)
        {

        }

        private void label6_Click(object sender, EventArgs e)
        {

        }

        private void button4_Click(object sender, EventArgs e)
        {
            start go = new start(); // кнопка розлогінування
            go.Show();
            this.Hide();
        }

        private void button5_Click(object sender, EventArgs e)
        {
            current_reg go = new current_reg(); // кнопка розлогінування
            go.Show();
            this.Hide();
        }
    }
}