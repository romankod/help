﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApplication1
{
    public partial class client_reg : Form
    {
        public client_reg()
        {
            InitializeComponent();
        }

        private void client_reg_Load(object sender, EventArgs e)
        {

        }

        private void textBox4_TextChanged(object sender, EventArgs e)
        {
        }

        private void button1_Click(object sender, EventArgs e)
        {
            // приймажмо змінні
            string firstName = textBox1.Text;
            string lastName = textBox2.Text;
            string login = textBox3.Text;
            string password = textBox4.Text;
            string money_st = textBox5.Text;
            // перевіряємо заповненість полів
            if (textBox1.Text != "" && textBox2.Text != "" && textBox3.Text !="" && textBox4.Text != "" && textBox4.Text != "" && textBox5.Text != "")
            {
                int money = Convert.ToInt32(money_st); // конвертуємо змінну
                Clients.CLient_reg(firstName, lastName, login, password, money);
                Login go = new Login(); // переходимо до наступної форми
                go.Show();
                this.Hide();
            }
            else
            {
                MessageBox.Show("Заповність всі поля");
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            start go = new start(); // кнопка назад
            go.Show();
            this.Hide();
        }

        private void textBox5_TextChanged(object sender, EventArgs e)
        {

        }

        private void button3_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }
    }
}
