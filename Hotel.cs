﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsFormsApplication1
{
    class Hotel
    {
        public string Name { get; set; }
        public string Adress { get; set; }
        public int roomCount { get; set; }
        public int price { get; set; }
        public Hotel()
        {

        }
        public Hotel(string Name, string Adress, int roomCount, int price)
        {
            this.Name = Name;
            this.Adress = Adress;
            this.roomCount = roomCount;
            this.price = price;
        }
        public static void Hotel_reg(string Name, string Adress, int roomCount, int price)
        {
            Data.Hotels.Add(new Hotel(Name, Adress, roomCount, price));
        }
    }


}
