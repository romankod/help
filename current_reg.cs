﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApplication1
{
    public partial class current_reg : Form
    {
        public current_reg()
        {
            InitializeComponent();
            textBox1.Text = CurrentUser.getInstance().currentclient.firstName.ToString(); // виводимо дані про поточного юзера
            textBox2.Text = CurrentUser.getInstance().currentclient.lastName.ToString();
            textBox5.Text = CurrentUser.getInstance().currentclient.money.ToString();
            textBox3.Text = CurrentUser.getInstance().currentclient.login.ToString();
            textBox4.Text = CurrentUser.getInstance().currentclient.password.ToString();
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void textBox2_TextChanged(object sender, EventArgs e)
        {

        }

        private void textBox3_TextChanged(object sender, EventArgs e)
        {

        }

        private void textBox4_TextChanged(object sender, EventArgs e)
        {

        }

        private void textBox5_TextChanged(object sender, EventArgs e)
        {

        }

        private void button2_Click(object sender, EventArgs e)
        {
            if (textBox1.Text != "" && textBox2.Text != "" && textBox3.Text != "" && textBox4.Text != "" && textBox5.Text != "")
            {
                string firstName_e = textBox1.Text;
                string lastName_e = textBox2.Text;
                string login_e = textBox3.Text;
                string password_e = textBox4.Text;
                string money_st = textBox5.Text; // записуєм логін
                int money_e = System.Convert.ToInt32(money_st);
                Clients client = new Clients(); // об'єкт класу
                bool successful = client.currentUserEdit( firstName_e,  lastName_e,  login_e,  password_e,  money_e); // викликаємо метод логінування і записуємо результат в змінну
                if (successful == true) // якщо логінування трушне, то переходимо далі
                {
                    Main go = new Main(); // переходимо до наступної форми
                    go.Show();
                    this.Hide();
                }
                else
                {
                    MessageBox.Show("такий логін уже зайнятий");
                }
            }
            else
            {
                MessageBox.Show("Перевірте поля");
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Main go = new Main(); // переходимо до наступної форми
            go.Show();
            this.Hide();
        }
    }
}


