﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsFormsApplication1
    {
        public class InitBD
        {
            public void AddSomeStaticStuff()
            {   // записуємо статичні готелі
                Data.Hotels.Add(new Hotel() { Name = "Галичина", Adress = "Грув стріт 13", roomCount = 122, price = 1000 });
                Data.Hotels.Add(new Hotel() { Name = "Тернопіль", Adress = "Тернопільська 2а", roomCount = 456, price = 2000 });
                Data.Hotels.Add(new Hotel() { Name = "Бам", Adress = "Володимира Великого 1", roomCount = 342, price = 3000 });
                Data.Hotels.Add(new Hotel() { Name = "Муха не сиділа", Adress = "Руська 155", roomCount = 234, price = 4000 });
                Data.Hotels.Add(new Hotel() { Name = "Бест оф зе бест", Adress = "Чорновола 3", roomCount = 133, price = 5000 });
                // записуємо статичних юзерів
                Data.Clients.Add(new Clients() { firstName = "Dmytro", lastName = "Romanko", login = "1", password="1",  money = 1000000000 });
                Data.Clients.Add(new Clients() { firstName = "Tyler", lastName = "Derden", login = "tyler", password = "fight",  money = 1000 });
                Data.Clients.Add(new Clients() { firstName = "Bob", lastName = "Paulson", login = "bob", password = "hisname",  money = 5000 });
                Data.Clients.Add(new Clients() { firstName = "Che", lastName = "Guevara", login = "che", password = "revolution",  money = 7000 });
                Data.Clients.Add(new Clients() { firstName = "Bob", lastName = "Marley", login = "music", password = "space",  money = 50600 });
        }
        }
}
